//global variables
var fileReader;
var selectedImg;
var DOTWEIGHT = [1, 16, 2, 32, 4, 64, 8, 128]; //left to right, top to bottom

function init() {
    fileReader = new FileReader();
}

function transformDot() {
    var canvas = document.getElementById("test");
    var width = selectedImg.width;
    var height = selectedImg.height;
    canvas.width = width;
    canvas.height = height;
    canvas.getContext("2d").drawImage(selectedImg, 0, 0, width, height);

    //create
    //select pixel array
    /* use getImageData and putImageData to change pixel*/
    //TODO: zoom in, 4 points as a block
    var ratio = 1;
    var context = canvas.getContext("2d");
    var result = "";
    console.log(context.getImageData(0, 0, 2 * ratio, 4 * ratio).data);

    for (var h = 0; h < height; h += 4 * ratio) {
        for (var w = 0; w < width; w += 2 * ratio) {
            //each 8 dots(blocks)
            var dotFactCounter = 10240;

            // var pixelArray = context.getImageData(w, h, 2 , 4 ).data;
            var pixelArray = context.getImageData(w, h, 2 * ratio, 4 * ratio).data;

            for (var i = 0; i < 8; i++) {
                var p = i * 4;
                var R = pixelArray[p];
                var G = pixelArray[p + 1];
                var B = pixelArray[p + 2];

                //threshold for dot
                if (RGB2Gray(R, G, B) < 160) {
                    dotFactCounter += DOTWEIGHT[i];
                }
            }
            result += String.fromCodePoint(dotFactCounter);
        }
        result += "\n";
    }

    document.getElementById("dotResult").innerHTML = result;


}

function blockBalance(pixelArray) {

}

//after select picture
function preloadPic() {
    var fileChooser = document.getElementById("picInput").files;

    //whether select pic
    if (fileChooser.length != 0) {
        var picFile = fileChooser[0];

        //reader finish parsing picture
        fileReader.onloadend = function(event) {
            //type transform
            var rawData = event.target.result;
            selectedImg = new Image();
            selectedImg.src = rawData; //need loading time

            //img loaded
            selectedImg.onload = function() {
                document.getElementById("processPic").disabled = false;
            };
        };

        //reader reads picture
        fileReader.readAsDataURL(picFile);
    } else {
        document.getElementById("processPic").disabled = true;
        /*cancel selection will remove previous picture, no sol yet*/
    }
}
window.onload = init;